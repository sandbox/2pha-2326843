(function ($) {
  Drupal.behaviors.cti = {
    attach: function (context, settings) {
      
      var continentP = $('#continent-progress');
      continentP.progressbar();
      var countryP = $('#country-progress');
      countryP.progressbar();
      var stateP = $('#state-progress');
      stateP.progressbar();
      var regionP = $('#region-progress');
      regionP.progressbar();
      var cityP = $('#city-progress');
      cityP.progressbar();
      
      $('#cti-admin').submit(function(){
        var vocab = cti_create_vocab();
        if (vocab) {
          cti_process(vocab);
        }
        return false;
      });
    }
  };
  
  function cti_console(message) {
    $('#edit-console').append(message+'&#13;&#10;');
  }
  
  function cti_create_vocab() {
    var vocab = false;
    if($('#edit-continent').is(':checked')||$('#edit-country').is(':checked')||
        $('#edit-state').is(':checked')||$('#edit-region').is(':checked')||$('#edit-city').is(':checked')){
      if($('#edit-cti-taxonomy-name').val().length && $('#edit-cti-taxonomy-machine-name').val().length) {
        $.ajax({
          async: false,
          url: Drupal.settings.basePath+'admin/config/content/cti/create_vocab',
          data: {name: $('#edit-cti-taxonomy-name').val(), machine: $('#edit-cti-taxonomy-machine-name').val()},
          success: function(data, textStatus, jqXHR){
            //console.log(data);
            cti_console(data.message);
            if (!data.error) {
              vocab = data.vocab;
            }
          },
          error: function(jqXHR, textStatus, errorThrown){$('#edit-console').append(textStatus);},
        });
      }else{
        cti_console('enter a vocabulary name');
      }
    }else{
      cti_console('atleast 1 component must be selected.');
    }
    return vocab;
  }
  
  function cti_process(vocab) {
    var countryparent = 0;
    var stateparet = 0;
    var regionparent = 0;
    var cityparent = 0;
    var continents = cti_get_children('6295630');
    $('#continent-progress').progressbar("option", "max", continents.totalResultsCount);
    for(var i = 0; i < continents.totalResultsCount; i++){
      $('#continent-message').html('Processing: '+continents.geonames[i].name);
      if($('#edit-continent').is(':checked')){
        // Create continent term.
        var contresults = cti_create_term(continents.geonames[i].name, 0, vocab.vid);
        if(!contresults.error){
          cti_console(contresults.message);
          countryparent = stateparent = regionparent = cityparent =contresults.tid;
        }
      }
      
      if($('#edit-country').is(':checked')||$('#edit-state').is(':checked')||$('#edit-region').is(':checked')||$('#edit-city').is(':checked')){
        var countries = cti_get_children(continents.geonames[i].geonameId);
        $('#country-progress').progressbar("option", "max", countries.totalResultsCount);
        for(var c = 0; c < countries.totalResultsCount; c++){
          $('#country-message').html('Processing: '+countries.geonames[c].name);
          if($('#edit-country').is(':checked')){
            // Create country term.
            var counresults = cti_create_term(countries.geonames[c].name, countryparent, vocab.vid);
            if(!counresults.error){
              cti_console(counresults.message);
              stateparent = regionparent = cityparent = counresults.tid;
            }
          }
          
          if($('#edit-state').is(':checked')||$('#edit-region').is(':checked')||$('#edit-city').is(':checked')){
            var states = cti_get_children(countries.geonames[c].geonameId);
            $('#state-progress').progressbar("option", "max", states.totalResultsCount);
            for(var s = 0; s < states.totalResultsCount; s++){
              $('#state-message').html('Processing: '+states.geonames[s].name);
              if($('#edit-state').is(':checked')){
                // Create state term
                var stateresults = cti_create_term(states.geonames[s].name, stateparent, vocab.vid);
                if(!stateresults.error){
                  cti_console(stateresults.message);
                  regionparent = cityparent = stateresults.tid;
                }
              }
              
              if($('#edit-region').is(':checked')||$('#edit-city').is(':checked')){
                var regions = cti_get_children(states.geonames[s].geonameId);
                $('#region-progress').progressbar("option", "max", regions.totalResultsCount);
                for(var r = 0; r < regions.totalResultsCount; r++){
                  $('#region-message').html('Processing: '+regions.geonames[r].name);
                  if($('#edit-region').is(':checked')){
                    // Create region term.
                    var regionresults = cti_create_term(region.geonames[r].name, regionparent, vocab.vid);
                    if(!regionresults.error){
                      cti_console(regionresults.message);
                      cityparent = regionresults.tid;
                    }
                  }
                  
                  if($('#edit-city').is(':checked')){
                    var cities = cti_get_children(regions.geonames[r].geonameId);
                    $('#city-progress').progressbar("option", "max", cities.totalResultsCount);
                    for(var ct = 0; ct < cities.totalResultsCount; ct++){
                      $('#city-message').html('Processing: '+cities.geonames[ct].name);
                      if($('#edit-city').is(':checked')){
                        // Create city term.
                        var cityresults = cti_create_term(cities.geonames[ct].name, cityparent, vocab.vid);
                        if(!cityresults.error){
                          cti_console(cityresults.message);
                        }
                      }
                      $('#city-progress').progressbar("value", ct+1);
                    }
                  }
                  $('#region-progress').progressbar("value", r+1);
                }
              }
              $('#state-progress').progressbar("value", s+1);
            }
          }
          $('#country-progress').progressbar("value", c+1);
        }
      }
      $('#continent-progress').progressbar( "value", i+1 );
    }
  }
  
  function cti_get_children(geonameId) {
    return JSON.parse($.ajax({
      type: "GET",
      async: false,
      url: 'http://www.geonames.org/childrenJSON?geonameId='+geonameId
    }).responseText);
  }
  
  function cti_create_term(term, parent, vid) {
    return JSON.parse($.ajax({
      type: "GET",
      async: false,
      url: Drupal.settings.basePath+'/admin/config/content/cti/create_term',
      data: {parent: parent, term: term, vid: vid},
    }).responseText);
  }
  
})(jQuery);